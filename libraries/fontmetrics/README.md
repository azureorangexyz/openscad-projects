# Fontmetrics

This library was developed by [arpruss](https://www.thingiverse.com/arpruss/designs) and shared on [Thingiverse](https://www.thingiverse.com/thing:3004457).

This library remains unchanged from when it was copyied on 2024-01-16.

This library is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
