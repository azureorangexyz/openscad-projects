use <./fontmetrics/fontmetrics.scad>;

// rotate as per a, v, but around point pt
module rotate_around(z, y, pt) {
    translate(pt)
        rotate([0, y, z]) // CHANGE HERE
            translate(-pt)
                children();   
}

//////////////////
// Measurements //

// main body
width = 120;                    // 1395 mm
depth = width;                  // 1395 mm
height = width * 1.530465950;   // 2135 mm

window_height = height * 0.2037470726;
window_width = width / 3.3;
window_depth = 2;

font = "Liberation Sans Narrow:style=Bold";
text_height = width / 12;

// base
base_width = width * 1.100358423;
base_depth = depth * 1.100358423;
base_height = height * 0.037470726;

// pyramid thingy on base top
pyramid_height = base_height * 0.4375;
pyramid_bottom_width = sqrt(base_width^2 + base_depth^2) / 2;
pyramid_top_width = sqrt(width^2 + depth^2) / 2;

body_shift = base_height + pyramid_height;

// top
top_height = height * 0.1077283372;
sign_height = top_height * 0.7391304348;
sign_width = 0.9*width;
sign_depth = 2;

top_shift = body_shift + height;

// top2
top2_height = height * 0.03981264637;
top2_width = 0.92 * width;
top2_depth = 0.92 * depth;
top2_shift = top_shift + top_height;

// top3
top3_height = height * 0.05152224824;
top3_width = 0.9 * top2_width;
top3_depth = 0.9 * top2_depth;
top3_shift = top2_shift + top2_height;

// doors
door_width = top3_width;

// point
point_width = width / 10;
point_depth = depth / 10;
ppart_height = height * 0.02341920375;

top3pyr_height = top3_height;
top3pyr_bottom_width = sqrt(top3_width^2 + top3_depth^2) / 2;
top3pyr_top_width = sqrt(point_width^2 + point_depth^2) / 2;
top3pyr_shift = top3_shift + top3_height;

point_shift = top3pyr_shift + top3pyr_height;

/////////////
// DRAWING //

// Base
module base() {
    translate([
        (width - base_width)/2,
        (depth - base_depth)/2,
        0
    ]) cube([base_width,base_depth,base_height]);
    
    // Pyramid thingy
    rotate_around(45, 0, [width/2,depth/2,0]) translate([
        (width)/2,
        (width)/2,
        base_height])
        cylinder(
            pyramid_height,
            pyramid_bottom_width,
            pyramid_top_width, $fn=4);
}

// main body
module main_body() {
    module windows() {
    //shifter = width - ((width / 2) + window_width) / 2;
    // shifter = (width - 2 * window_width) / 5;
    shifter = (width - door_width) / 2 + (door_width - 2 * window_width) / 5;
    shifter2 = 1.5* (door_width - 2 * window_width) / 5;
        for(i = [1 : 4])
            translate([width - shifter - window_width,0,i*height/25 + (i-1)*height/5])
                window();
        for(i = [1 : 4])
            translate([(width / 2) - shifter2 - window_width,0,i*height/25 + (i-1)*height/5])
                window();
        
    };
    
    module window() {
        cube([window_width,window_depth,window_height]);
    };
    
    translate([0,0,body_shift]) difference() {
        cube([width,depth,height]);
        for (j = [0 : 90 : 270])
            rotate_around(j, 0, [width/2,depth/2,0]) windows();
    }
}

// top
module top() {
    sign_shift = (width - sign_width) / 2;

    police_text = "POLICE";
    police_height = text_height;
    police_width = measureText(police_text, font=font, size=police_height);


    box_text = "BOX";
    box_height = police_height;
    box_width = measureText(box_text, font=font, size=box_height);

    public_text = "PUBLIC";
    public_height = police_height / 3;
    public_width = measureText(public_text, font=font, size=public_height);

    call_text = "CALL";
    call_height = police_height / 3;
    call_width = measureText(call_text, font=font, size=call_height);

    module top_piece() { 
        cube([width,depth,top_height]);
        for (j = [0 : 90 : 270])
            rotate_around(j, 0, [width/2,depth/2,0]) signs();
        
    }
    module writing(t,f,s,shift) {
        translate([shift,0,0])
            rotate([90,0,0])
                linear_extrude(2)
                    text(t, font=f, size=s);
    }
    module print(text_shift) {
        translate([text_shift,0,0])
            writing(police_text, font, police_height, 0);
        translate([2*text_shift,0,police_height - public_height])
            writing(public_text, font, public_height, police_width);
        call_shift = police_width + (public_width - call_width) / 2;
        translate([2*text_shift,0,0])
            writing(call_text, font, call_height, call_shift);
        translate([3*text_shift,0,0])
            writing(box_text, font, box_height, police_width + public_width);
    }
    module signs() {
        translate([(width - sign_width)/2,-sign_depth,0])
            difference() {
                cube([sign_width,sign_depth,sign_height]);
                text_shift = (sign_width - police_width - public_width - box_width) / 6;
                translate([text_shift,2,(sign_height - police_height) / 2])
                    print(text_shift);
            }
    }
    
    translate([0,0,top_shift]) top_piece();
}

module roof() {
    // top2
    translate([
        (width - top2_width)/2,
        (depth - top2_depth)/2,
        top2_shift
    ]) cube([top2_width,top2_depth,top2_height]);
    
    // top2
    translate([
        (width - top3_width)/2,
        (depth - top3_depth)/2,
        top3_shift
    ]) cube([top3_width,top3_depth,top3_height]);
    
    rotate_around(45, 0, [width/2,depth/2,0]) translate([
        (width)/2,
        (width)/2,
        top3pyr_shift])
        cylinder(
            top3pyr_height,
            top3pyr_bottom_width,
            top3pyr_top_width, $fn=4);
    
    // point
    module point() {
        cube([point_width, point_depth, ppart_height]);
        translate([.1*point_width, .1*point_depth, ppart_height])
            cube([.8*point_width, .8*point_depth, 3*ppart_height]);
        translate([0,0,4*ppart_height])
            cube([point_width, point_depth, ppart_height]);
    }
    translate([
        (width - point_width)/2,
        (depth - point_depth)/2,
        point_shift
    ]) point();
}


//render(convexity = 2) {
    union() {
        base();
        main_body();
        top();
        roof();
    }
//}











