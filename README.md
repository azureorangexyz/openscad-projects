# OpenSCAD and I

I'm fairly new to OpenSCAD and we're just starting to get along.

This repo holds all my OpenSCAD projects with the corresponding files:

- project file (.scad)
- rendering for 3d printing (.stl)
- screenshots (.png)

## [Tardis](./tardis)

The Tardis (Time And Relative Dimensions In Space) is the Doctor's best friend and trusted time machine.

The writings are made using the [fontmetrics library](./libraries/fontmetrics) which I found on Thingiverse.

### [version 1](./tardis/v1)

My first time ever using OpenSCAD, I tried to model the Tardis of some image I found online.
All lengths are calculated from the width of the Tardis' main body cuboid.

![tardis-v1.png](./tardis/v1/tardis-v1-f3d.png)
